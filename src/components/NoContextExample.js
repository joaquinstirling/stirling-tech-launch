import React from 'react';

export default class NoContextExample extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lang: 'en',
            theme: 'dark'
        }

        this.onClickChangeTheme = this.onClickChangeTheme.bind(this);
        this.onClickChangeLang = this.onClickChangeLang.bind(this);
    }
    
    onClickChangeLang() {
        let lang = this.state.lang === 'en' ? 'de' : 'en';
        this.setState({lang: lang})
    }
   
    onClickChangeTheme() {
        let theme = this.state.theme === 'dark' ? 'light' : 'dark';
        console.log(theme)
        if (theme !== 'light') {
            document.body.className = 'dark'
        } else {
            document.body.className = 'light'
        }
        this.setState({theme: theme})
    }

    render() {
        let theme = this.state.theme;
        let lang = this.state.lang;
        
        return <Content theme={theme} lang={lang} onClickChangeTheme={this.onClickChangeTheme} onClickChangeLang={this.onClickChangeLang} />;
    }
  }
  
  function Content(props) {
    // El componenete Content toma un prop
    // y lo pasa a todos sus hijos
    // tiene que ser pasado por todos los componentes
    return (
      <div>
        <ChangeTheme lang={props.lang} theme={props.theme} onClickChangeTheme={props.onClickChangeTheme} />
        <ChangeLang lang={props.lang} onClickChangeLang={props.onClickChangeLang} />
      </div>
    );
  }

class ChangeTheme extends React.Component {
    render() {
        return (
            <div>
                <h2>{this.props.lang === 'en' ? "Change Theme" : "Thema ändern"}</h2>
                <button onClick={() => this.props.onClickChangeTheme()} theme={this.props.theme}>{this.props.lang === 'en' ? "Change Theme" : "Thema ändern"} </button>
            </div>
        );
    }
}

class ChangeLang extends React.Component {
    render() {
        return (
            <div>
                <h2>{this.props.lang === 'en' ? "Change Lang" : "Lang ändern"}</h2>
                <h3>{this.props.lang}</h3>
                <button onClick={() => this.props.onClickChangeLang()} >{this.props.lang === 'en' ? "Change Lang" : "Lang ändern"}</button>
            </div>
        );
    }
}