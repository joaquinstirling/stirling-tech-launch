import React from 'react';
import AppContext from '../context'

export default class AppProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            'lang': 'en',
            'theme': 'dark',
            'onClickChangeTheme': this.onClickChangeTheme,
            'onClickChangeLang': this.onClickChangeLang,
        }

        this.onClickChangeTheme = this.onClickChangeTheme.bind(this);
        this.onClickChangeLang = this.onClickChangeLang.bind(this);
    }
    
    onClickChangeLang = () => {
        let lang = this.state.lang === 'en' ? 'de' : 'en';
        this.setState({lang: lang})
    }
   
    onClickChangeTheme = () => {
        let theme = this.state.theme === 'dark' ? 'light' : 'dark';
        console.log(theme)
        if (theme !== 'light') {
            document.body.className = 'dark'
        } else {
            document.body.className = 'light'
        }
        this.setState({theme: theme})
    }

    render() {
        return <AppContext.Provider value={this.state}>{this.props.children}</AppContext.Provider>;
    }
  }