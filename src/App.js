import React from 'react';
import './App.css';

import AppProvider from './components/AppProvider';
import AppContext from './context'
import NoContextExample from './components/NoContextExample';

function App() {
  return (
    <div className="App">
    <AppProvider>
      <AppContext.Consumer>
        {appContext => (
          <Content appContext={appContext} />
        )}
      </AppContext.Consumer>
    </AppProvider>
    </div>
  );
}

export default App;



  
function Content(props) {
  console.log(props.appContext);
  return (
    <div>
      <ChangeTheme appContext={props.appContext}/>
      <ChangeLang appContext={props.appContext}/>
    </div>
  );
}

class ChangeTheme extends React.Component {
  constructor(props) {
    super(props);
    // this.props.appContext.onClickChangeTheme = this.onClickChangeTheme.bind(this)
  }

  render() {
    let lang = this.props.appContext.lang; 
    console.log(this.props)
      return (
          <div>
              <h2>{lang === 'en' ? "Change Theme" : "Thema ändern"}</h2>
              <button onClick={() => this.props.appContext.onClickChangeTheme()}>{lang === 'en' ? "Change Theme" : "Thema ändern"} </button>
          </div>
      );
  }
}

class ChangeLang extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let lang = this.props.appContext.lang; 
      return (
          <div>
              <h2>{lang === 'en' ? "Change Lang" : "Lang ändern"}</h2>
              <h3>{lang}</h3>
              <button onClick={() => this.props.appContext.onClickChangeLang()} >{lang === 'en' ? "Change Lang" : "Lang ändern"}</button>
          </div>
      );
  }
}