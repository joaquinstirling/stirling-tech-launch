import React from "react";

const AppContext = React.createContext({
	lang: '', // use this to set the lang. change hour formatting. 
    theme: '', // use this to change between dark and light
});

// The following is just an arbitrary name to be rendered and displayed in DOM and React Component Tools
AppContext.displayName = "AppContext";
export default AppContext;